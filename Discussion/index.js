console.log("Thanks Lord!");

console.log("hi");

console.log(document);
// result: document HTML code

const txtFirstName = document.querySelector("#txt-first-name");
console.log(txtFirstName);
// result: input field / tag

/*
	alternative ways:
		>> document.getElementById("txt-first-name");
		>> document.getElementByClassName("text-class");
		>> document.getElementByTagName("h1");
*/

// target the full name
let spanFullName = document.querySelector("#span-full-name");

// event listeners
// (event) can have a shorthand of (e)
// addEventListener('stringOfAnEvent', function)
// innerHTML allows us to call the tag and change the value dynamically
// Each HTML element has an innerHTML property that defines both the HTML code and the text that occurs between that element's opening and closing tag. By changing an element's innerHTML after some user interaction, you can make much more interactive pages.

txtFirstName.addEventListener('keyup', (event) => {
	spanFullName.innerHTML = txtFirstName.value;
});

txtFirstName.addEventListener('keyup', (event) => {
	console.log(event);
	console.log(event.target);
	console.log(event.target.value);
})

// stretch
const keyCodeEvent = (e) => {
	let kc = e.keyCode;
	if(kc === 65){
		e.target.value = null;
		alert('Someone clicked a');
	}
}

